﻿/// <reference path="views/main.ts" />
class app extends curly.Container {
    private main: views.Main;
    private config: {};

    constructor() {
        // super method has an overload, if you pass true in the second parameter it will add the container to the body of the DOM
        super("app", true);
        
        var appWidth: number = curly.Window.width;
        var appHeight: number = curly.Window.height;

        var dataNode: HTMLElement = document.getElementById("data");
        this.config = JSON.parse(dataNode.textContent);

        var fontRatio: number = (0.2 / 1000) ;

        if (appWidth < 1000) {
            document.body.style.fontSize = ((fontRatio * appWidth) + 0.8 ) + "em";
        }

        // for font to load     
        var fontLoader = new window["FontLoader"](["Open Sans"], this, 3000);
        fontLoader.loadFonts();

    }

    private fontLoaded(fontFamily) {
        // One of the fonts was loaded
        console.log("font loaded: " + fontFamily);
    }

    private fontsLoaded(error) {
        if (error !== null) {
            // Reached the timeout but not all fonts were loaded
            console.log(error.message);
            console.log(error.notLoadedFontFamilies);

            this.main = new views.Main(this.config);
            this.addChild(this.main);
            this.main.init();
        } else {
            // All fonts were loaded
            console.log("all fonts were loaded");
            this.main = new views.Main(this.config);
            this.addChild(this.main);
            this.main.init();

        }
        
    }

}

window.onload = () => {
    var begin = new app();
};