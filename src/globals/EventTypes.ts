﻿module globals
{
    export class EventTypes {
        static listItemClicked: string = "LIST_ITEM_CLICKED";
        static backToList: string = "BACK_TO_LIST";

    }
}

