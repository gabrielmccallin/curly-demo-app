﻿module globals{
    export module styles {
        export class Style {
            
            static smallText = {
                fontSize: "0.8em",
                fontFamily:"Open Sans",
                fontWeight: "400",
                color: "#000000",
                letterSpacing: "0em"
            }

            static body = {
                fontSize: "1em",
                fontFamily:"Open Sans",
                fontWeight: "400",
                color: "#000000",
                letterSpacing: "0em"
            }

            static bodyLight = {
                fontSize: "1em",
                fontFamily: "Open Sans",
                fontWeight: "300",
                color: "#000000",
                letterSpacing: "0em"
            }
            
            static h3 = {
                fontSize: "1.2em",
                fontFamily:"Open Sans",
                fontWeight: "400",
                color: "#000000",
                letterSpacing: "0em"
            }

            static h2 = {
                fontSize: "2.5em",
                fontFamily:"Open Sans",
                fontWeight: "300",
                color: "#ffffff",
                letterSpacing: "-0.05em"
            }

            static h1 = {
                fontSize: "4em",
                fontFamily: "Open Sans",
                fontWeight: "300",
                color: "#ffffff",
                letterSpacing: "0em"
            }


            static colours = {
                blue: "#4444ee",
                lightgrey:"#eeeeee"
            }

            static list = {
                rollOver: "#cccccc",
                rollOut: "#eeeeee",
                bgColour: "#eeeeee",
                selected: "#ccccff"
            }

            static site = {
                margin: 20,
                navWidth: 175,
                twoColumnBreakPoint: 700,
                oneColumnBreakPoint: 400
            }

            static images = {
                mailIcon: "images/mailIcon.png",
                backButton: "images/noun_134078.png"
            }
        }
    }
} 