﻿module views {
    export module panel {
        export class Panel extends curly.Container {
            private config: {};
            private bg: curly.Rectangle;
            private sender: curly.TextField;
            private receiver: curly.TextField;
            private subject: curly.TextField;
            private message: curly.TextField;
            private backButton: curly.Image;

            constructor(config: {}) {
                super("Panel");

                this.config = config;
            }

            init(): void {

                this.bg = new curly.Rectangle(100, 100, globals.styles.Style.list.bgColour);
                //this.addChild(this.bg);

                var senderVars = new curly.TextFieldVars();
                senderVars.text = "";
                for (var i in globals.styles.Style.h3) {
                    senderVars[i] = globals.styles.Style.h3[i];
                }

                this.sender = new curly.TextField(senderVars);
                this.addChild(this.sender);
                this.sender.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                var receiverVars = new curly.TextFieldVars();
                receiverVars.text = "to " + this.config["name"];
                for (var i in globals.styles.Style.bodyLight) {
                    receiverVars[i] = globals.styles.Style.bodyLight[i];
                }

                this.receiver = new curly.TextField(receiverVars);
                this.addChild(this.receiver);
                this.receiver.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                var subjectVars = new curly.TextFieldVars();
                subjectVars.text = "";
                for (var i in globals.styles.Style.h3) {
                    subjectVars[i] = globals.styles.Style.h3[i];
                }
                subjectVars.color = globals.styles.Style.colours.blue;

                this.subject = new curly.TextField(subjectVars);
                this.addChild(this.subject);
                this.subject.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                var messageVars = new curly.TextFieldVars();
                messageVars.text = "";
                for (var i in globals.styles.Style.body) {
                    messageVars[i] = globals.styles.Style.body[i];
                }

                this.message = new curly.TextField(messageVars);
                this.addChild(this.message);
                this.message.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                this.backButton = new curly.Image("backButton");
                this.addChild(this.backButton);
                this.backButton.load(globals.styles.Style.images.backButton);
                this.backButton.set({
                    x: curly.Window.width,
                    y: 35,
                    width: 30,
                    height: 30,
                    cursor: "pointer"
                });
                this.backButton.addDomEventListener(this, "click", this.backClicked);

            }

            private backClicked(e: Event) {
                var event = new curly.Event(globals.EventTypes.backToList, this);
                curly.Observer.dispatchEvent(event);
            }

            update(data: any) {
                this.sender.setText(data.sender);

                this.receiver.set({
                    y: this.sender.y + this.sender.height
                });

                this.subject.setText(data.subject);
                this.subject.set({
                    y: this.receiver.y + this.receiver.height + (globals.styles.Style.site.margin)
                });


                this.message.setText(data.message);
                this.message.set({
                    y: this.subject.y + this.subject.height + globals.styles.Style.site.margin
                });
            }

            resize(width: number, height: number) {
                //this.nav.width = curly.Window.width;

                this.bg.width = width;

                this.sender.set({
                    width: width - (globals.styles.Style.site.margin * 4)
                });

                this.receiver.set({
                    y: this.sender.y + this.sender.height,
                    width: width - (globals.styles.Style.site.margin * 4)
                });

                this.subject.set({
                    y: this.receiver.y + this.receiver.height + (globals.styles.Style.site.margin ),
                    width: width - (globals.styles.Style.site.margin * 4)
                });
                this.message.set({
                    y: this.subject.y + this.subject.height + globals.styles.Style.site.margin,
                    width: width - (globals.styles.Style.site.margin * 4)
                });

            }

            showBack() {
                this.backButton.set({
                    x: globals.styles.Style.site.margin
                });

            }

            hideBack() {
                this.backButton.set({
                    x: curly.Window.width
                });
            }

        }
    }
}