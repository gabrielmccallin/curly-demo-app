﻿/// <reference path="nav.ts" />
/// <reference path="list/list.ts" />
module views {
    export class Main extends curly.Container {
        private config: {};
        private preloader: HTMLElement;
        private nav: views.Nav;
        private list: views.list.List;
        private panel: views.panel.Panel;
        private data: any[];
        private layoutState: number;
        private firstTime: boolean = true;

        constructor(config: {}) {
            super("main");
            
            this.config = config;
        }

        init(): void {

            window.onresize = () => this.resize();

            curly.Observer.addEventListener(this, globals.EventTypes.listItemClicked, this.itemClicked);

            curly.Observer.addEventListener(this, globals.EventTypes.backToList, this.backClicked);

            this.nav = new views.Nav(this.config);
            this.addChild(this.nav);
            this.nav.init();

            this.list = new views.list.List(this.config);
            this.addChild(this.list);
            this.list.init();

            this.panel = new views.panel.Panel(this.config);
            this.addChild(this.panel);
            this.panel.init();


            this.resize();

            this.loadData();
        }


        private loadData() {
            var loader = new curly.URLLoader();
            loader.addEventListener(this, curly.URLLoader.COMPLETE, this.dataLoaded);
            loader.load("data.json", "GET", {}, this);
        }

        private dataLoaded(e: curly.Event) {
            this.data = JSON.parse(e.getData()).data;

            this.update(this.data);

        }

        private resize() {
            if (curly.Window.width > globals.styles.Style.site.twoColumnBreakPoint) {
                this.threeColumnLayout();
                this.layoutState = 2;
            }
            else {
                //if (curly.Window.width > globals.styles.Style.site.oneColumnBreakPoint) {
                if (this.firstTime) {
                    this.twoColumnLayoutListView();
                }
                else {
                    this.twoColumnLayoutMessageView();
                }
                this.layoutState = 1;
                //}
                //else {
                //    this.oneColumnLayout();
                //    this.layoutState = 0;
                //}
            }
        }

        private threeColumnLayout() {
            this.nav.set({
                x: 0
            });
            this.nav.resize(globals.styles.Style.site.navWidth, curly.Window.height);

            var columnWidth = (curly.Window.width - globals.styles.Style.site.navWidth) / 2;

            this.list.set({
                x: globals.styles.Style.site.navWidth
            });
            this.list.resize(columnWidth, curly.Window.height);

            this.panel.set({
                x: columnWidth + globals.styles.Style.site.navWidth,
                width: columnWidth,
                height: curly.Window.height,
                //overflowY: "auto",
                overflowX: "hidden"
            });
            this.panel.resize(columnWidth, curly.Window.height);
            this.panel.hideBack();
        }

        private twoColumnLayoutListView() {
            this.nav.set({
                x: 0
            });
            this.list.set({
                x: globals.styles.Style.site.navWidth
            });

            this.nav.resize(globals.styles.Style.site.navWidth, curly.Window.height);

            var columnWidth = curly.Window.width - globals.styles.Style.site.navWidth;

            this.list.resize(columnWidth, curly.Window.height);

            this.panel.set({
                x: curly.Window.width
            });

        }

        private twoColumnLayoutMessageView() {

            this.panel.showBack();

            this.nav.set({
                x: curly.Window.width
            });
            this.list.set({
                x: curly.Window.width
            });

            this.panel.set({
                x: 0,
                width: curly.Window.width,
                height: curly.Window.height,
                overflowX: "hidden"
            });
            this.panel.resize(curly.Window.width, curly.Window.height);
        }

        private oneColumnLayout() {

        }

        private update(data:any[]) {
            this.list.update(data);
        }

        private itemClicked(e:curly.Event) {
            console.log("observer FIRE");
            for (var i in this.data) {
                if (this.data[i].id === e.getData()) {
                    this.panel.update(this.data[i]);
                    break;
                }
            }
            switch (this.layoutState) {
                case 1:
                    if (!this.firstTime){
                        this.twoColumnLayoutMessageView();
                    }
                    this.firstTime = false;
                    break;
                default:
                    break;

            }
        }

        private backClicked(e: curly.Event) {
            this.twoColumnLayoutListView();
        }

       

    }
}