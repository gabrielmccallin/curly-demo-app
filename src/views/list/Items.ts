﻿/// <reference path="../../globals/config.ts" />
module views {
    export module list {
        export class Items extends curly.Container {
            private config: globals.Config;
            private nav: views.Nav;
            private itemsTracker: Array<views.list.Item>;
            private selectedItem: Item;
            private itemsWidth: number;


            constructor(config: globals.Config) {
                super("Items");

                this.config = config;

            }

            init(): void {

            }

            update(data: any[]) {
                this.itemsTracker = new Array();
                var yPos = 0;

                for (var i = 0; i < data.length; i++) {
                    var item = new views.list.Item(data[i]);
                    this.addChild(item);
                    item.addEventListener(this, globals.EventTypes.listItemClicked, this.itemClicked);
                    item.init();

                    item.set({
                        y: yPos,
                        height: "auto",
                        width: "auto"
                    });

                    item.resize(this.itemsWidth, 0);

                    yPos += item.height + 1;

                    this.itemsTracker.push(item);

                }

                this.selectedItem = this.itemsTracker[0];
                this.selectedItem.select();
                var event = new curly.Event(globals.EventTypes.listItemClicked, this, data[0].id);

                curly.Observer.dispatchEvent(event);
            }

            resize(width:number, height:number) {
                this.itemsWidth = width;
                if (this.itemsTracker) {
                    for (var i in this.itemsTracker) {
                        this.itemsTracker[i].resize(width, height);
                    }
                }
            }

            private itemClicked(e: curly.Event) {
                if (this.selectedItem) {
                    this.selectedItem.enable();
                }
                this.selectedItem = e.getTarget();
                curly.Observer.dispatchEvent(e);
            }



        }
    }
}