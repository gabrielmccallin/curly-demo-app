﻿/// <reference path="items.ts" />
module views {
    export module list {
        export class List extends curly.Container {
            private config: {};
            private items: views.list.Items; 
            private bg: curly.Rectangle;

            constructor(config: {}) {
                super("List");

                this.config = config;
            }

            init(): void {

                this.bg = new curly.Rectangle(100, 99, globals.styles.Style.list.bgColour);
                this.addChild(this.bg);

                var titleVars = new curly.TextFieldVars();
                titleVars.text = "Inbox";
                for (var i in globals.styles.Style.h2) {
                    titleVars[i] = globals.styles.Style.h2[i];
                }
                titleVars.color = globals.styles.Style.colours.blue;

                var title = new curly.TextField(titleVars);
                this.addChild(title);
                title.set({
                    y: globals.styles.Style.site.margin,
                    x: globals.styles.Style.site.margin
                });

                this.items = new views.list.Items(null);
                this.addChild(this.items);
                this.items.init();
                this.items.set({
                    y: 100,
                    //overflowY: "scroll",
                    overflowX: "hidden"
                });

                //this.items.element.style.overflow = "scroll";


            }

            update(data:any[]) {
                this.items.update(data);
            }

            resize(width:number, height:number) {
                //this.nav.width = curly.Window.width;

                this.bg.width = width;

                this.items.resize(width, height);

                this.items.set({
                    height: height - 100,
                    width: width
                });
            }




        }
    }
}