﻿module views {
    export module list {
        export class Item extends curly.Container {
            private config: any;
            private title: curly.TextField;
            private subject: curly.TextField;
            private bg: curly.Rectangle;
            private button: curly.LabelButton;


            constructor(config: any) {
                super("Item");

                this.config = config;
            }

            init(): void {

                this.bg = new curly.Rectangle(2000, 65, globals.styles.Style.list.bgColour);
                this.addChild(this.bg);

                var vars = new curly.LabelButtonVars();
                vars.bgColourOut = globals.styles.Style.list.rollOut;
                vars.bgColourOver = globals.styles.Style.list.rollOver;
                vars.bgSelectedColour = globals.styles.Style.list.selected;
                vars.height = 65;
                vars.width = 100;
                vars.text = "";

                this.button = new curly.LabelButton(vars);
                this.addChild(this.button);
                this.button.init();
                this.button.addEventListener(this, curly.LabelButton.CLICK, this.itemClicked);

                var titleVars = new curly.TextFieldVars();
                titleVars.text = this.config.sender;
                for (var i in globals.styles.Style.h3) {
                    titleVars[i] = globals.styles.Style.h3[i];
                }
                titleVars.height = 28;
                titleVars.width = 100;

                this.title = new curly.TextField(titleVars);
                this.addChild(this.title);
                this.title.set({
                    y: 5,
                    x: globals.styles.Style.site.margin,
                    pointerEvents: "none"
                });

                var subjectVars = new curly.TextFieldVars();
                subjectVars.text = this.config.subject;
                for (var i in globals.styles.Style.body) {
                    subjectVars[i] = globals.styles.Style.body[i];
                }
                subjectVars.height = 25;
                subjectVars.width = 100;
                subjectVars.color = globals.styles.Style.colours.blue;

                this.subject = new curly.TextField(subjectVars);
                this.addChild(this.subject);
                this.subject.set({
                    y: 35,
                    x: globals.styles.Style.site.margin,
                    pointerEvents: "none"
                });

            }

            private itemClicked(e: curly.Event) {

                this.subject.set({
                    color: globals.styles.Style.body.color
                });

                var event = new curly.Event(globals.EventTypes.listItemClicked, this, this.config.id);
                this.dispatchEvent(event);

                this.select();
            }

            resize(width:number, height:number) {
                this.subject.set({
                    width:width
                });
                this.title.set({
                    width: width
                });
                this.button.width = width;
            }

            enable() {

                this.button.enable();
            }

            select() {
                this.button.select();

                this.subject.set({
                    color: globals.styles.Style.bodyLight.color,
                    fontWeight: globals.styles.Style.bodyLight.fontWeight
                });
            }




        }
    }
}