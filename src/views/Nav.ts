﻿module views {
    export class Nav extends curly.Container {
        private config: {};
        private bg: curly.Rectangle;

        constructor(config: {}) {
            super("nav");
            
            this.config = config;
        }

        init(): void {

            this.bg = new curly.Rectangle(curly.Window.width * 0.2, curly.Window.height, globals.styles.Style.colours.blue);
            this.addChild(this.bg);

            var titleVars = new curly.TextFieldVars();
            titleVars.text = "Mail";
            for (var i in globals.styles.Style.h2) {
                titleVars[i] = globals.styles.Style.h2[i];
            }

            var title = new curly.TextField(titleVars);
            this.addChild(title);
            title.set({
                y: globals.styles.Style.site.margin,
                x: globals.styles.Style.site.margin
            });

            var mailIcon = new curly.Image();
            this.addChild(mailIcon);
            mailIcon.load(globals.styles.Style.images.mailIcon);
            mailIcon.set({
                y: 100,
                x: globals.styles.Style.site.margin
            });

            var unreadVars = new curly.TextFieldVars();
            for (var i in globals.styles.Style.body) {
                unreadVars[i] = globals.styles.Style.body[i];
            }
            unreadVars.color = "#ffffff";
            unreadVars.text = "11";

            var unreadCount = new curly.TextField(unreadVars);
            this.addChild(unreadCount);
            unreadCount.set({
                y: 99,
                x: (globals.styles.Style.site.margin ) + mailIcon.width + 10
            });


        }

        resize(width:number, height:number) {
            this.bg.width = width;
            this.bg.height = height;
        }

       



    }
}