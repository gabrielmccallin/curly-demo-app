# README #

### An example web application built with **{curly}**, a JavaScript library for programming the web. ###

Forget <markup> and use {code} instead. Program instead of jumping between script, CSS and HTML markup. 

This application is written in Typescript and includes examples of:

- **Containers** (by extending curly.Container)  

- **Textfield component**

- **Image component**  

- **Button component**  

- **Responsive layout**  

- **Styles as static classes**  

- **Grouping elements into classes**  

- **Namespaces**  

- **Observers**, for routing events at an application level


The entry point for the application is app.ts. 

All classes are compiled to app.js.

A Visual Studio solution is also provided.