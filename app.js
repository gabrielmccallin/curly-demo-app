﻿var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var views;
(function (views) {
    var Nav = (function (_super) {
        __extends(Nav, _super);
        function Nav(config) {
            _super.call(this, "nav");

            this.config = config;
        }
        Nav.prototype.init = function () {
            this.bg = new curly.Rectangle(curly.Window.width * 0.2, curly.Window.height, globals.styles.Style.colours.blue);
            this.addChild(this.bg);

            var titleVars = new curly.TextFieldVars();
            titleVars.text = "Mail";
            for (var i in globals.styles.Style.h2) {
                titleVars[i] = globals.styles.Style.h2[i];
            }

            var title = new curly.TextField(titleVars);
            this.addChild(title);
            title.set({
                y: globals.styles.Style.site.margin,
                x: globals.styles.Style.site.margin
            });

            var mailIcon = new curly.Image();
            this.addChild(mailIcon);
            mailIcon.load(globals.styles.Style.images.mailIcon);
            mailIcon.set({
                y: 100,
                x: globals.styles.Style.site.margin
            });

            var unreadVars = new curly.TextFieldVars();
            for (var i in globals.styles.Style.body) {
                unreadVars[i] = globals.styles.Style.body[i];
            }
            unreadVars.color = "#ffffff";
            unreadVars.text = "11";

            var unreadCount = new curly.TextField(unreadVars);
            this.addChild(unreadCount);
            unreadCount.set({
                y: 99,
                x: (globals.styles.Style.site.margin) + mailIcon.width + 10
            });
        };

        Nav.prototype.resize = function (width, height) {
            this.bg.width = width;
            this.bg.height = height;
        };
        return Nav;
    })(curly.Container);
    views.Nav = Nav;
})(views || (views = {}));
/// <reference path="../../globals/config.ts" />
var views;
(function (views) {
    (function (list) {
        var Items = (function (_super) {
            __extends(Items, _super);
            function Items(config) {
                _super.call(this, "Items");

                this.config = config;
            }
            Items.prototype.init = function () {
            };

            Items.prototype.update = function (data) {
                this.itemsTracker = new Array();
                var yPos = 0;

                for (var i = 0; i < data.length; i++) {
                    var item = new views.list.Item(data[i]);
                    this.addChild(item);
                    item.addEventListener(this, globals.EventTypes.listItemClicked, this.itemClicked);
                    item.init();

                    item.set({
                        y: yPos,
                        height: "auto",
                        width: "auto"
                    });

                    item.resize(this.itemsWidth, 0);

                    yPos += item.height + 1;

                    this.itemsTracker.push(item);
                }

                this.selectedItem = this.itemsTracker[0];
                this.selectedItem.select();
                var event = new curly.Event(globals.EventTypes.listItemClicked, this, data[0].id);

                curly.Observer.dispatchEvent(event);
            };

            Items.prototype.resize = function (width, height) {
                this.itemsWidth = width;
                if (this.itemsTracker) {
                    for (var i in this.itemsTracker) {
                        this.itemsTracker[i].resize(width, height);
                    }
                }
            };

            Items.prototype.itemClicked = function (e) {
                if (this.selectedItem) {
                    this.selectedItem.enable();
                }
                this.selectedItem = e.getTarget();
                curly.Observer.dispatchEvent(e);
            };
            return Items;
        })(curly.Container);
        list.Items = Items;
    })(views.list || (views.list = {}));
    var list = views.list;
})(views || (views = {}));
/// <reference path="items.ts" />
var views;
(function (views) {
    (function (list) {
        var List = (function (_super) {
            __extends(List, _super);
            function List(config) {
                _super.call(this, "List");

                this.config = config;
            }
            List.prototype.init = function () {
                this.bg = new curly.Rectangle(100, 99, globals.styles.Style.list.bgColour);
                this.addChild(this.bg);

                var titleVars = new curly.TextFieldVars();
                titleVars.text = "Inbox";
                for (var i in globals.styles.Style.h2) {
                    titleVars[i] = globals.styles.Style.h2[i];
                }
                titleVars.color = globals.styles.Style.colours.blue;

                var title = new curly.TextField(titleVars);
                this.addChild(title);
                title.set({
                    y: globals.styles.Style.site.margin,
                    x: globals.styles.Style.site.margin
                });

                this.items = new views.list.Items(null);
                this.addChild(this.items);
                this.items.init();
                this.items.set({
                    y: 100,
                    //overflowY: "scroll",
                    overflowX: "hidden"
                });
                //this.items.element.style.overflow = "scroll";
            };

            List.prototype.update = function (data) {
                this.items.update(data);
            };

            List.prototype.resize = function (width, height) {
                //this.nav.width = curly.Window.width;
                this.bg.width = width;

                this.items.resize(width, height);

                this.items.set({
                    height: height - 100,
                    width: width
                });
            };
            return List;
        })(curly.Container);
        list.List = List;
    })(views.list || (views.list = {}));
    var list = views.list;
})(views || (views = {}));
/// <reference path="nav.ts" />
/// <reference path="list/list.ts" />
var views;
(function (views) {
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main(config) {
            _super.call(this, "main");
            this.firstTime = true;

            this.config = config;
        }
        Main.prototype.init = function () {
            var _this = this;
            window.onresize = function () {
                return _this.resize();
            };

            curly.Observer.addEventListener(this, globals.EventTypes.listItemClicked, this.itemClicked);

            curly.Observer.addEventListener(this, globals.EventTypes.backToList, this.backClicked);

            this.nav = new views.Nav(this.config);
            this.addChild(this.nav);
            this.nav.init();

            this.list = new views.list.List(this.config);
            this.addChild(this.list);
            this.list.init();

            this.panel = new views.panel.Panel(this.config);
            this.addChild(this.panel);
            this.panel.init();

            this.resize();

            this.loadData();
        };

        Main.prototype.loadData = function () {
            var loader = new curly.URLLoader();
            loader.addEventListener(this, curly.URLLoader.COMPLETE, this.dataLoaded);
            loader.load("data.json", "GET", {}, this);
        };

        Main.prototype.dataLoaded = function (e) {
            this.data = JSON.parse(e.getData()).data;

            this.update(this.data);
        };

        Main.prototype.resize = function () {
            if (curly.Window.width > globals.styles.Style.site.twoColumnBreakPoint) {
                this.threeColumnLayout();
                this.layoutState = 2;
            } else {
                //if (curly.Window.width > globals.styles.Style.site.oneColumnBreakPoint) {
                if (this.firstTime) {
                    this.twoColumnLayoutListView();
                } else {
                    this.twoColumnLayoutMessageView();
                }
                this.layoutState = 1;
                //}
                //else {
                //    this.oneColumnLayout();
                //    this.layoutState = 0;
                //}
            }
        };

        Main.prototype.threeColumnLayout = function () {
            this.nav.set({
                x: 0
            });
            this.nav.resize(globals.styles.Style.site.navWidth, curly.Window.height);

            var columnWidth = (curly.Window.width - globals.styles.Style.site.navWidth) / 2;

            this.list.set({
                x: globals.styles.Style.site.navWidth
            });
            this.list.resize(columnWidth, curly.Window.height);

            this.panel.set({
                x: columnWidth + globals.styles.Style.site.navWidth,
                width: columnWidth,
                height: curly.Window.height,
                //overflowY: "auto",
                overflowX: "hidden"
            });
            this.panel.resize(columnWidth, curly.Window.height);
            this.panel.hideBack();
        };

        Main.prototype.twoColumnLayoutListView = function () {
            this.nav.set({
                x: 0
            });
            this.list.set({
                x: globals.styles.Style.site.navWidth
            });

            this.nav.resize(globals.styles.Style.site.navWidth, curly.Window.height);

            var columnWidth = curly.Window.width - globals.styles.Style.site.navWidth;

            this.list.resize(columnWidth, curly.Window.height);

            this.panel.set({
                x: curly.Window.width
            });
        };

        Main.prototype.twoColumnLayoutMessageView = function () {
            this.panel.showBack();

            this.nav.set({
                x: curly.Window.width
            });
            this.list.set({
                x: curly.Window.width
            });

            this.panel.set({
                x: 0,
                width: curly.Window.width,
                height: curly.Window.height,
                overflowX: "hidden"
            });
            this.panel.resize(curly.Window.width, curly.Window.height);
        };

        Main.prototype.oneColumnLayout = function () {
        };

        Main.prototype.update = function (data) {
            this.list.update(data);
        };

        Main.prototype.itemClicked = function (e) {
            console.log("observer FIRE");
            for (var i in this.data) {
                if (this.data[i].id === e.getData()) {
                    this.panel.update(this.data[i]);
                    break;
                }
            }
            switch (this.layoutState) {
                case 1:
                    if (!this.firstTime) {
                        this.twoColumnLayoutMessageView();
                    }
                    this.firstTime = false;
                    break;
                default:
                    break;
            }
        };

        Main.prototype.backClicked = function (e) {
            this.twoColumnLayoutListView();
        };
        return Main;
    })(curly.Container);
    views.Main = Main;
})(views || (views = {}));
/// <reference path="views/main.ts" />
var app = (function (_super) {
    __extends(app, _super);
    function app() {
        // super method has an overload, if you pass true in the second parameter it will add the container to the body of the DOM
        _super.call(this, "app", true);

        var appWidth = curly.Window.width;
        var appHeight = curly.Window.height;

        var dataNode = document.getElementById("data");
        this.config = JSON.parse(dataNode.textContent);

        var fontRatio = (0.2 / 1000);

        if (appWidth < 1000) {
            document.body.style.fontSize = ((fontRatio * appWidth) + 0.8) + "em";
        }

        // for font to load
        var fontLoader = new window["FontLoader"](["Open Sans"], this, 3000);
        fontLoader.loadFonts();
    }
    app.prototype.fontLoaded = function (fontFamily) {
        // One of the fonts was loaded
        console.log("font loaded: " + fontFamily);
    };

    app.prototype.fontsLoaded = function (error) {
        if (error !== null) {
            // Reached the timeout but not all fonts were loaded
            console.log(error.message);
            console.log(error.notLoadedFontFamilies);

            this.main = new views.Main(this.config);
            this.addChild(this.main);
            this.main.init();
        } else {
            // All fonts were loaded
            console.log("all fonts were loaded");
            this.main = new views.Main(this.config);
            this.addChild(this.main);
            this.main.init();
        }
    };
    return app;
})(curly.Container);

window.onload = function () {
    var begin = new app();
};
var globals;
(function (globals) {
    var EventTypes = (function () {
        function EventTypes() {
        }
        EventTypes.listItemClicked = "LIST_ITEM_CLICKED";
        EventTypes.backToList = "BACK_TO_LIST";
        return EventTypes;
    })();
    globals.EventTypes = EventTypes;
})(globals || (globals = {}));
var globals;
(function (globals) {
    (function (styles) {
        var Style = (function () {
            function Style() {
            }
            Style.smallText = {
                fontSize: "0.8em",
                fontFamily: "Open Sans",
                fontWeight: "400",
                color: "#000000",
                letterSpacing: "0em"
            };

            Style.body = {
                fontSize: "1em",
                fontFamily: "Open Sans",
                fontWeight: "400",
                color: "#000000",
                letterSpacing: "0em"
            };

            Style.bodyLight = {
                fontSize: "1em",
                fontFamily: "Open Sans",
                fontWeight: "300",
                color: "#000000",
                letterSpacing: "0em"
            };

            Style.h3 = {
                fontSize: "1.2em",
                fontFamily: "Open Sans",
                fontWeight: "400",
                color: "#000000",
                letterSpacing: "0em"
            };

            Style.h2 = {
                fontSize: "2.5em",
                fontFamily: "Open Sans",
                fontWeight: "300",
                color: "#ffffff",
                letterSpacing: "-0.05em"
            };

            Style.h1 = {
                fontSize: "4em",
                fontFamily: "Open Sans",
                fontWeight: "300",
                color: "#ffffff",
                letterSpacing: "0em"
            };

            Style.colours = {
                blue: "#4444ee",
                lightgrey: "#eeeeee"
            };

            Style.list = {
                rollOver: "#cccccc",
                rollOut: "#eeeeee",
                bgColour: "#eeeeee",
                selected: "#ccccff"
            };

            Style.site = {
                margin: 20,
                navWidth: 175,
                twoColumnBreakPoint: 700,
                oneColumnBreakPoint: 400
            };

            Style.images = {
                mailIcon: "images/mailIcon.png",
                backButton: "images/noun_134078.png"
            };
            return Style;
        })();
        styles.Style = Style;
    })(globals.styles || (globals.styles = {}));
    var styles = globals.styles;
})(globals || (globals = {}));
var views;
(function (views) {
    (function (list) {
        var Item = (function (_super) {
            __extends(Item, _super);
            function Item(config) {
                _super.call(this, "Item");

                this.config = config;
            }
            Item.prototype.init = function () {
                this.bg = new curly.Rectangle(2000, 65, globals.styles.Style.list.bgColour);
                this.addChild(this.bg);

                var vars = new curly.LabelButtonVars();
                vars.bgColourOut = globals.styles.Style.list.rollOut;
                vars.bgColourOver = globals.styles.Style.list.rollOver;
                vars.bgSelectedColour = globals.styles.Style.list.selected;
                vars.height = 65;
                vars.width = 100;
                vars.text = "";

                this.button = new curly.LabelButton(vars);
                this.addChild(this.button);
                this.button.init();
                this.button.addEventListener(this, curly.LabelButton.CLICK, this.itemClicked);

                var titleVars = new curly.TextFieldVars();
                titleVars.text = this.config.sender;
                for (var i in globals.styles.Style.h3) {
                    titleVars[i] = globals.styles.Style.h3[i];
                }
                titleVars.height = 28;
                titleVars.width = 100;

                this.title = new curly.TextField(titleVars);
                this.addChild(this.title);
                this.title.set({
                    y: 5,
                    x: globals.styles.Style.site.margin,
                    pointerEvents: "none"
                });

                var subjectVars = new curly.TextFieldVars();
                subjectVars.text = this.config.subject;
                for (var i in globals.styles.Style.body) {
                    subjectVars[i] = globals.styles.Style.body[i];
                }
                subjectVars.height = 25;
                subjectVars.width = 100;
                subjectVars.color = globals.styles.Style.colours.blue;

                this.subject = new curly.TextField(subjectVars);
                this.addChild(this.subject);
                this.subject.set({
                    y: 35,
                    x: globals.styles.Style.site.margin,
                    pointerEvents: "none"
                });
            };

            Item.prototype.itemClicked = function (e) {
                this.subject.set({
                    color: globals.styles.Style.body.color
                });

                var event = new curly.Event(globals.EventTypes.listItemClicked, this, this.config.id);
                this.dispatchEvent(event);

                this.select();
            };

            Item.prototype.resize = function (width, height) {
                this.subject.set({
                    width: width
                });
                this.title.set({
                    width: width
                });
                this.button.width = width;
            };

            Item.prototype.enable = function () {
                this.button.enable();
            };

            Item.prototype.select = function () {
                this.button.select();

                this.subject.set({
                    color: globals.styles.Style.bodyLight.color,
                    fontWeight: globals.styles.Style.bodyLight.fontWeight
                });
            };
            return Item;
        })(curly.Container);
        list.Item = Item;
    })(views.list || (views.list = {}));
    var list = views.list;
})(views || (views = {}));
var views;
(function (views) {
    (function (panel) {
        var Panel = (function (_super) {
            __extends(Panel, _super);
            function Panel(config) {
                _super.call(this, "Panel");

                this.config = config;
            }
            Panel.prototype.init = function () {
                this.bg = new curly.Rectangle(100, 100, globals.styles.Style.list.bgColour);

                //this.addChild(this.bg);
                var senderVars = new curly.TextFieldVars();
                senderVars.text = "";
                for (var i in globals.styles.Style.h3) {
                    senderVars[i] = globals.styles.Style.h3[i];
                }

                this.sender = new curly.TextField(senderVars);
                this.addChild(this.sender);
                this.sender.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                var receiverVars = new curly.TextFieldVars();
                receiverVars.text = "to " + this.config["name"];
                for (var i in globals.styles.Style.bodyLight) {
                    receiverVars[i] = globals.styles.Style.bodyLight[i];
                }

                this.receiver = new curly.TextField(receiverVars);
                this.addChild(this.receiver);
                this.receiver.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                var subjectVars = new curly.TextFieldVars();
                subjectVars.text = "";
                for (var i in globals.styles.Style.h3) {
                    subjectVars[i] = globals.styles.Style.h3[i];
                }
                subjectVars.color = globals.styles.Style.colours.blue;

                this.subject = new curly.TextField(subjectVars);
                this.addChild(this.subject);
                this.subject.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                var messageVars = new curly.TextFieldVars();
                messageVars.text = "";
                for (var i in globals.styles.Style.body) {
                    messageVars[i] = globals.styles.Style.body[i];
                }

                this.message = new curly.TextField(messageVars);
                this.addChild(this.message);
                this.message.set({
                    y: 100,
                    x: globals.styles.Style.site.margin
                });

                this.backButton = new curly.Image("backButton");
                this.addChild(this.backButton);
                this.backButton.load(globals.styles.Style.images.backButton);
                this.backButton.set({
                    x: curly.Window.width,
                    y: 35,
                    width: 30,
                    height: 30,
                    cursor: "pointer"
                });
                this.backButton.addDomEventListener(this, "click", this.backClicked);
            };

            Panel.prototype.backClicked = function (e) {
                var event = new curly.Event(globals.EventTypes.backToList, this);
                curly.Observer.dispatchEvent(event);
            };

            Panel.prototype.update = function (data) {
                this.sender.setText(data.sender);

                this.receiver.set({
                    y: this.sender.y + this.sender.height
                });

                this.subject.setText(data.subject);
                this.subject.set({
                    y: this.receiver.y + this.receiver.height + (globals.styles.Style.site.margin)
                });

                this.message.setText(data.message);
                this.message.set({
                    y: this.subject.y + this.subject.height + globals.styles.Style.site.margin
                });
            };

            Panel.prototype.resize = function (width, height) {
                //this.nav.width = curly.Window.width;
                this.bg.width = width;

                this.sender.set({
                    width: width - (globals.styles.Style.site.margin * 4)
                });

                this.receiver.set({
                    y: this.sender.y + this.sender.height,
                    width: width - (globals.styles.Style.site.margin * 4)
                });

                this.subject.set({
                    y: this.receiver.y + this.receiver.height + (globals.styles.Style.site.margin),
                    width: width - (globals.styles.Style.site.margin * 4)
                });
                this.message.set({
                    y: this.subject.y + this.subject.height + globals.styles.Style.site.margin,
                    width: width - (globals.styles.Style.site.margin * 4)
                });
            };

            Panel.prototype.showBack = function () {
                this.backButton.set({
                    x: globals.styles.Style.site.margin
                });
            };

            Panel.prototype.hideBack = function () {
                this.backButton.set({
                    x: curly.Window.width
                });
            };
            return Panel;
        })(curly.Container);
        panel.Panel = Panel;
    })(views.panel || (views.panel = {}));
    var panel = views.panel;
})(views || (views = {}));
//# sourceMappingURL=app.js.map
